@extends('_layouts.master')

@section('title', 'About')

@section('content')
<article>
    <section>
        <h1>About Me, Suryasa Made 🇮🇩</h1>

        <p>I am a highly driven and passionate person for data and software development, with expertise
            lies in Backend and Fullstack Development with some stacks such as Laravel + PostgreSQL + ReactJS + and
            many
            other tools.
            With tight competition with
            thousand of applicants, I managed to be accepted as one of the scholarship of <a
                href="https://grow.google/intl/id_id/bangkit/">Bangkit Academy</a> by Google
            Indonesia, and completed the Mobile Development learning-path 🤖.</p>

        <div id="bitly_cv_link">
            <a href="https://bit.ly/cv-suryasamade" target="_blank" id="cv_link">The CV (Curricullum Vitae)</a>
            <small>or scan qrcode below</small>
            <img src="/bit.ly_cv-suryasamade.jpeg" alt="qr-code-bitly-cv-suryasamade" id="cv_qr_code">
        </div>

        <h2>Skills 🛠</h2>
        @foreach ($page->homeContent->about->tags as $tag)
        <a href="/tags/{{ $tag }}" class="skills">
            <h3>{{ $tag }}</h3>
        </a>
        @endforeach
    </section>
</article>
@endsection