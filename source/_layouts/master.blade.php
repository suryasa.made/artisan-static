<!DOCTYPE html>
<html lang="{{ $page->language ?? 'en' }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>
        @yield('title')
        {{ !empty($__env->yieldContent('title')) ? ' | ' : '' }}
        {{ $page->site->title }}
    </title>

    @include('_partials.head.favicon')
    @include('_partials.head.meta')
    @include('_partials.cms.identity_widget')

    <link rel="stylesheet" href="{{ mix('css/main.css', 'assets/build') }}">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100&display=swap" rel="stylesheet">

</head>

<body>
    <section>
        <header>
            <nav>
                <strong
                    style="font-size:40px; font-family: 'Poppins', sans-serif; border: 1px solid black; padding: 5px 20px">
                    {{ $page->site->title }}
                </strong><br>
                <div style="margin-bottom: 20px"></div>
                <ul>
                    <li><a href="/" class="menu-nav">Home</a></li>
                    <li><a href="/projects" class="menu-nav">Projects</a></li>
                    <li><a href="/posts" class="menu-nav">Posts</a></li>
                    <li><a href="/about" class="menu-nav">About</a></li>
                    <li><a href="/contact" class="menu-nav">Contact</a></li>
                </ul>
            </nav>
        </header>

        {{-- <article>
            <section>
                @yield('content')
            </section>
        </article> --}}
        <div>
            @yield('content')
        </div>

        <footer id="footer" style="text-align:center">
            <nav>
                <ul>
                    <li><a href="mailto:{{ $page->owner->email }}" id="email">Email</a></li>
                    <li><a href="https://www.linkedin.com/in/{{ $page->owner->linkedin }}/" target="_blank">LinkedIn</a>
                    </li>
                    <li><a href="https://github.com/{{ $page->owner->github }}/" target="_blank">GitHub</a></li>
                    <li><a href="https://medium.com/{{ $page->owner->medium }}/" target="_blank">Medium</a></li>
                    <li><a href="https://twitter.com/{{ $page->owner->twitter }}/" target="_blank">Twitter</a></li>
                </ul>
            </nav>
            <small>
                &#169; Designed & built by <a href="/">Suryasa Made</a> / 2024 /
                maintained by <a href="https://github.com/raniesantos/artisan-static">Ranie Santos.</a>
            </small>
        </footer>
    </section>

    <script src="{{ mix('js/main.js', 'assets/build') }}"></script>
    @includeWhen($page->production, '_partials.analytics')
    @include('_partials.cms.identity_redirect')
</body>

</html>