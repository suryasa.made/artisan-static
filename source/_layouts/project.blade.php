@extends('_layouts.master')

@section('title', $page->title)

{{--
konten pada portofolio project idealnya berisi beberapa poin berikut:
1. title
2. short brief
3. role / contributions
4. problem + solution
5. tech / stacks / tools / architecture
6. features
7. link
8. images
9. achievements: number of users, business growth, etc
--}}

@section('content')
<article class="no-border-article">
    <section>
        <h1>{{ $page->title }}</h1>

        @if ($page->image)
        {{-- <p>{{ $page->image }}</p> --}}
        <div class="post-header">
            <img src="{{ $page->image }}">
        </div>
        @endif

        <p>
            <strong>{{ $page->prettyDate('F j, Y') }}</strong><br>
            @foreach ($page->tags as $tag)
            <a href="/tags/{{ $tag }}">{{ $tag }}</a>
            {{ $loop->last ? '' : '-' }}
            @endforeach
        </p>

        {{-- <blockquote data-phpdate="{{ $page->date }}">
            <em>WARNING: This post is over a year old. Some of the information this contains may be outdated.</em>
        </blockquote>

        <hr>

        <p>DISCLAIMER: Any 3rd-party services in these posts and in the config are only recommendations/suggestions. I
            am not
            affiliated with any of them.</p> --}}

        @yield('projectContent')

        {{--
        <hr> --}}

        @include('_partials.share')

        {{-- @if ($page->comments)
        @include('_partials.comments')
        @else
        <p>Comments are not enabled for this post.</p>
        @endif --}}
    </section>
</article>
@endsection