@extends('_layouts.master')

@section('title', 'Home')

@section('content')
<article class="no-border-article">
    <section>
        <h1>A Software Engineer: Discover a solution, let the business run smooth and grow 📈</h1>

        {{-- <strong>Help the clients to discover solutions and let the business run smooth and grow</strong> --}}

        {{-- short about --}}
        <div class="home-section">
            <h2>
                <a class="nav-section-link" href="/about">Hello, I am Suryasa Made ✋
                    <span><i class="bi bi-link-45deg"></i></span></a>
            </h2>
            <p>I am a highly driven and passionate person for data and software development, with expertise
                lies in Backend and Fullstack Development with some stacks such as Laravel + PostgreSQL + ReactJS + and
                many
                other tools.
                With tight competition with
                thousand of applicants, I managed to be accepted as one of the scholarship of <a
                    href="https://grow.google/intl/id_id/bangkit/">Bangkit Academy</a> by Google
                Indonesia, and completed the Mobile Development learning-path 🤖.</p>
        </div>

        {{-- short projects --}}
        <div class="home-section">
            <h2>
                <a class="nav-section-link" href="/projects">Some of Featured Projects 💻
                    <span><i class="bi bi-link-45deg"></i></span></a>
                </a>
            </h2>

            <p>Most of my work is contract based, tending to freelance and build projects to learn new skills to adapt
                to current industry needs 🌱. Focused on Backend Engineer area, but have an interest on Mobile
                Development
                too.</p>

            {{--
            konten pada card portofolio project berisi poin-poin berikut:
            1. title
            2. short brief
            4. tech / stacks / tools / architecture
            6. link
            7. images
            --}}

            {{-- card start --}}
            <div class="card-container">
                @foreach ($page->homeContent->portos as $portos)
                <a href="{{ $portos->link }}" target="_blank" class="col">
                    <div class="overlay"></div>
                    <img src={{ $portos->imageUrl }} alt='{{ $portos->imageAlt }}' />
                    <div class="desc-group">
                        @foreach ($portos->tech as $tech)
                        <code>{{ $tech }}</code>
                        @endforeach
                        <p class="title"><strong>{{ $portos->title }}</strong></p>
                        <p class="desc">{{ $portos->shortDesc }}</p>
                    </div>
                </a>
                @endforeach
            </div>
            {{-- card end --}}
        </div>

        <p>Learning new skills and conducting experiments is important as a Software Engineer to stay relevant in
            technological developments. Stay curious and go beyond.</p>

        {{-- short posts --}}
        <div class="home-section">
            <h2>
                <a class="nav-section-link" href="/posts">Various Publications 📃
                    <span><i class="bi bi-link-45deg"></i></span></a>
                </a>
            </h2>

            <p>Every trip we make deserves to be remembered, and every knowledge gained should be immortalized by
                writing. The ups and downs of life always build us towards the future.
            </p>

            <blockquote>
                That Which Does Not Kill Us Makes Us Stronger
                <footer>— Friedrich Nietzsche (Twilight of the Idols)</footer>
            </blockquote>

            <p>The following is some of simple articles that I have make/write, hopefully can give the readers new
                insights 📝.</p>

            {{-- card start --}}
            <div class="card-container">
                @foreach ($page->homeContent->articles as $articles)
                <a href="{{ $articles->link }}" target="_blank" class="col">
                    <div class="overlay"></div>
                    <img src={{ $articles->imageUrl }} alt='{{ $articles->imageAlt }}' />
                    <div class="desc-group">
                        @foreach ($articles->platform as $platform)
                        <code>{{ $platform }}</code>
                        @endforeach
                        <p class="title"><strong>{{ $articles->title }}</strong></p>
                        <p class="desc">{{ $articles->date }}</p>
                    </div>
                </a>
                @endforeach
            </div>
            {{-- card end --}}
        </div>

        {{-- short contact --}}
        <div class="home-section">
            <p>TLDR; I am open to joining impactful projects / companies. If you are seeking a dedicated
                professional as a partner to collaborate
                with, message and lets connect with
                me - on <a href="https://www.linkedin.com/in/{{ $page->owner->linkedin }}/" target="_blank">LinkedIn</a>
                / <a href="https://github.com/{{ $page->owner->github }}" target="_blank">GitHub</a> / <a
                    href="https://medium.com/{{ $page->owner->medium }}" target="_blank">Medium</a> / <a
                    href="https://twitter.com/{{ $page->owner->twitter }}" target="_blank">Twitter</a>.
                Together we explore
                opportunities to make a meaningful impact in the futures ✨.</p>
        </div>






        <!-- <p>Artisan Static is a starter template for building a static Jigsaw blog hosted on Netlify.</p> -->

        {{-- <p>This blog come to keep my writing and also to show the world wide</p> --}}

        <!-- <p>This comes with code highlighting, share buttons, comments, analytics, a contact form, a CMS and more.</p> -->

        <!-- <p>This template has extremely minimal HTML, CSS and JavaScript, which makes the code easy to build on top of or replace completely.</p> -->

        {{-- <h2 id="test">Asset Compilation Test</h2>

        <p>CSS test: <span class="test-css">text with blue border</span></p>

        <p>JavaScript test: <a href="#test" class="test-js">click me</a></p> --}}
    </section>
</article>

<hr>

@endsection