@extends('_layouts.master')

@section('title', 'Projects')

@section('content')
<article>
    <section>
        <h1>Featured Projects 💻</h1>
        {{--
        konten pada portofolio project idealnya berisi beberapa poin berikut:
        1. title
        2. short brief
        3. problem + solution
        4. tech / stacks / tools / architecture
        5. features
        6. link
        7. images
        8. achievements: number of users, business growth, etc
        --}}

        <p>Most of my work is contract based, tending to freelance and build projects to learn new skills to adapt to
            current industry needs 🌱. Focused on Backend Engineer area, but have an interest on Mobile Development too.
            Learning new skills and conducting experiments is important as a Software Engineer to stay relevant in
            technological developments. Stay curious and go beyond.
        </p>

        <div class="card-container">
            @forelse ($projects->sortByDesc('date') as $project)
            @php $projectId = explode('/', $project->getPath())[2]; @endphp
            <a href="{{ $project->getPath() }}" target="_blank" id="{{ $projectId }}" class="card-item">
                <img src="{{ $project->image }}" alt="{{ $project->altImg }}" />

                <div class="desc-group">
                    <h2>{{ $project->title }}</h2>
                    @foreach ($project->tags as $tag)
                    <code>{{ $tag }}</code>
                    @endforeach
                    <p class="desc">{{ $project->desc }}</p>
                </div>
            </a>
            @empty
            <p>No projects to show.</p>
            @endforelse
        </div>

    </section>
</article>
@endsection