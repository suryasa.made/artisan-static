---
title: "Lorem ipsum dolor sit amet consectetur."
# limit the desc on: 30 words!
desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed earum repellendus ullam asperiores ipsa? Saepe molestiae accusamus deserunt voluptatibus repudiandae quasi iure odio, recusandae at, facilis ab quam sed commodi."
date: 2020-04-21
tags:
  - css
  - javascript
  - php
  - laravel
image: https://culturedvultures.com/wp-content/uploads/2022/09/Dragon-Ball-Z-803x452.jpeg
altImg: "image anu anu"
---

## Heading 2

### Heading 3

**Bold Text**

_Italic Text_

This is [an example](http://example.com/ "Title") inline link.

[This link](http://example.net/) has no title attribute.

Image - Jigsaw logo

![Jigsaw logo](https://cloud.githubusercontent.com/assets/357312/25055001/5603687e-212e-11e7-8fad-0b33dbf7fb71.png)

Lorem ipsum dolor, sit amet consectetur adipisicing elit. Perspiciatis non, iusto ad quis assumenda architecto veritatis tenetur expedita provident unde ex hic deserunt quasi blanditiis excepturi minus fugit recusandae? Obcaecati, quae impedit veritatis eum sed officia quis nihil expedita molestias quas hic porro dicta dolor eaque repellat, deleniti veniam quod, maxime maiores molestiae doloribus saepe. Dolores harum, alias perspiciatis quaerat unde voluptas eum reiciendis neque explicabo dolor at, sapiente non exercitationem hic, vero illum quasi deleniti. Minus autem illum voluptatum exercitationem tempora, fuga sed numquam sapiente quos accusantium ipsa veritatis, consequuntur animi harum quis obcaecati suscipit asperiores quae? Necessitatibus neque laboriosam dolor molestiae eum, vero non sed? Cupiditate nostrum enim dolorum.

- Top-level item 1
  - Nested item
  - Nested item
- Top-level item 2
  - Nested item
  - Nested item
- Top-level item 3
  - Nested item
  - Nested item

1. Top-level item 1
   1. Nested item
   2. Nested item
2. Top-level item 2
   1. Nested item
   2. Nested item
3. Top-level item 3
   1. Nested item
   2. Nested item

Term
: Description of the term
: Another description of the term

Term
Synonym
: Description of the term

> This is a quote

This is some inline-code: `echo something`
