@extends('_layouts.master')

@section('title', 'Posts')

@section('content')
<article>
    <section>
        <h1>Posts & Various Publications 📃</h1>

        <p>Every trip we make deserves to be remembered, and every knowledge gained should be immortalized by
            writing. The ups and downs of life always build us towards the future. The following is some of simple
            articles that I have make/write, hopefully can give the readers new
            insights 📝.</p>

        <div class="card-container">
            @forelse ($posts->sortByDesc('title') as $post)
            @php $postId = explode('/', $post->getPath())[2]; @endphp
            <a href="{{ $post->getPath() }}" target="_blank" id="{{ $postId }}" class="card-item">
                <img src="{{ $post->image }}" alt="{{ $post->altImg }}" />

                <div class="desc-group">
                    <h2>{{ $post->title }}</h2>
                    @foreach ($post->tags as $tag)
                    <code>{{ $tag }}</code>
                    @endforeach
                    <p class="desc">{{ $post->desc }}</p>
                </div>
            </a>
            @empty
            <p>No posts to show.</p>
            @endforelse
        </div>

        {{-- <ul>
            <li> --}}
                {{-- explorations --}}
                {{-- @foreach ($post->tags as $tag)
                <code>{{ $tag }}</code>
                @endforeach --}}
                {{-- <img src="{{ $post->image }}" style="object-fit: cover; height: 250px; width: 100%;"> --}}
                {{-- <a href="{{ $post->getPath() }}">{{ $post->title }}</a>
                <small>{{ $post->prettyDate() }}</small>
            </li> --}}
            {{--
        </ul>
        @empty
        <p>No posts to show.</p>
        @endforelse --}}
    </section>
</article>
@endsection