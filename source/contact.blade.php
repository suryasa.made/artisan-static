@extends('_layouts.master')

@section('title', 'Contact')

@section('content')
<article>
    <section>
        <h1>Contact Me 👇</h1>

        {{-- programmer account need to add on contact -> like leetcode, kaggle, stackoverflow--}}

        <form action="https://formcarry.com/s/{{ $page->services->formcarry }}" method="POST"
            enctype="multipart/form-data">
            <div>
                <label for="sender">Name</label><br>
                <input type="text" name="sender" class="input" id="sender" autofocus required>
            </div>

            <div>
                <label for="email">Email</label><br>
                <input type="email" name="email" class="input" id="email" required>
            </div>

            <div>
                <label for="subject">Subject</label><br>
                <input type="text" name="subject" class="input" id="subject" required>
            </div>

            <div>
                <label for="message">Message</label><br>
                <textarea name="message" id="message" class="input" required></textarea>
            </div>

            <input type="text" name="_gotcha" style="display: none;">

            <input type="submit" name="submit" class="input" value="Send">
        </form>
    </section>
</article>
@endsection