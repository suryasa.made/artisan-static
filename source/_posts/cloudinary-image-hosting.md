---
title: '05 - Cloudinary Image Hosting'
date: 2020-05-05
image: https://res.cloudinary.com/artisanstatic/photos.jpg
comments: false
---
Register on [Cloudinary](https://cloudinary.com/invites/lpov9zyyucivvxsnalc5/qq2slabgpy590znlop4j).

Go to `config.php`, add your **cloud name** and **API key** under the `services` key.

Committing image files into Git is generally a bad idea unless you're sure that they will very rarely get edited/updated. That is why it is ideal to host images externally with services like Cloudinary when using static site generators.
