---
title: "08 - Netlify CMS"
desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed earum repellendus ullam asperiores ipsa? Saepe molestiae accusamus deserunt voluptatibus repudiandae quasi iure odio, recusandae at, facilis ab quam sed commodi."
date: 2020-05-08
tags:
  - css
  - javascript
image: https://res.cloudinary.com/artisanstatic/cms.jpg
altImg: "image anu anu"
comments: false
---

The included files related to Netlify CMS should work out of the box.

Just make sure that your Netlify Identity settings are correctly configured.

You can change the Netlify CMS version being used in `config.php` under the `services` key.
