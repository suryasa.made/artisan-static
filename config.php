<?php

return [
    'production' => false,
    'baseUrl' => 'https://madmadmade.netlify.app',
    'site' => [
        'title' => 'Suryasa Made',
        'description' => 'Personal blog of Suryasa Made.',
        'image' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1920px-Image_created_with_a_mobile_phone.png',
    ],
    'owner' => [
        'name' => 'I Made Suryasa',
        'nickName' => 'Suryasa',
        'email' => 'suryasa.made@gmail.com',
        'twitter' => 'suryasa___',
        'github' => 'suryasamade',
        'medium' => '@suryasamade',
        'linkedin' => 'madesuryasa',
    ],
    'services' => [
        'cmsVersion' => '2.10.48',
        'analytics' => 'UA-166318621-1',
        'disqus' => 'artisanstatic',
        'formcarry' => '36ueEkO4TeX',
        'cloudinary' => [
            'cloudName' => 'artisanstatic',
            'apiKey' => '365895137117119',
        ],
    ],
    'collections' => [
        'posts' => [
            'path' => 'posts/{filename}',
            'sort' => '-date',
            'extends' => '_layouts.post',
            'section' => 'postContent',
            'isPost' => true,
            'comments' => true,
            'tags' => [],
            'prettyDate' => function ($page, $format = 'M j, Y') {
                return date($format, $page->date);
            },
        ],
        'projects' => [
            'path' => 'projects/{filename}',
            'sort' => '-date',
            'extends' => '_layouts.project',
            'section' => 'projectContent',
            'isPost' => true,
            'comments' => true,
            'tags' => [],
            'prettyDate' => function ($page, $format = 'M j, Y') {
                return date($format, $page->date);
            },
        ],
        'tags' => [
            'path' => 'tags/{filename}',
            'extends' => '_layouts.tag',
            'section' => '',
            'name' => function ($page) {
                return $page->getFilename();
            },
        ],
    ],
    'homeContent' => [
        'portos' => [
            [
                'title' => 'Lorem ne bro',
                'shortDesc' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rem ut architecto recusandae provident, facere sunt modi molestiae voluptatum iure ad.',
                'tech' => ['tech1', 'tech2'],
                'link' => 'projects#dummy-project-copy',
                'imageUrl' => 'https://miro.medium.com/v2/resize:fit:2000/1*cOFZH56G3dnpp18yMhwgHw.png',
                'imageAlt' => 'blarrr oooo blaaar'
            ],
            [
                'title' => 'Lorem ne bro 2',
                'shortDesc' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, expedita tempora? Illum magnam porro facere aliquid quisquam ab non itaque autem ducimus aspernatur! Non, optio?',
                'tech' => ['tech1'],
                'link' => 'projects#dummy-project',
                'imageUrl' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1920px-Image_created_with_a_mobile_phone.png',
                'imageAlt' => 'bla bla'
            ],
            [
                'title' => 'Lorem ne bro 3',
                'shortDesc' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe nam illum aspernatur voluptates expedita aperiam illo dolores sed ut ad. Totam praesentium similique illum.',
                'tech' => ['tech1', '2', '3'],
                'link' => 'projects#project-3',
                'imageUrl' => 'https://culturedvultures.com/wp-content/uploads/2022/09/Dragon-Ball-Z-803x452.jpeg',
                'imageAlt' => 'bla bla'
            ],
        ],
        'articles' => [
            [
                'title' => 'Article ne bro',
                'date' => 'Wed, January 24 2024',
                'platform' => ['platform1', 'platform2'],
                'link' => 'posts#article-1',
                'imageUrl' => 'https://miro.medium.com/v2/resize:fit:2000/1*cOFZH56G3dnpp18yMhwgHw.png',
                'imageAlt' => 'blarrr oooo blaaar'
            ],
            [
                'title' => 'Lorem ipsum dolor sit amet consectetur',
                'date' => 'Tue, January 23 2024',
                'platform' => ['platform1'],
                'link' => 'posts#article-2',
                'imageUrl' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1920px-Image_created_with_a_mobile_phone.png',
                'imageAlt' => 'bla bla'
            ],
            [
                'title' => 'Article ne bro 3',
                'date' => 'Mon, January 21 2024',
                'platform' => ['platform1', '2', '3'],
                'link' => 'posts#article-3',
                'imageUrl' => 'https://culturedvultures.com/wp-content/uploads/2022/09/Dragon-Ball-Z-803x452.jpeg',
                'imageAlt' => 'bla bla'
            ],
        ],
        'about' => [
            'tags' => ['php', 'laravel', 'css', 'javascript']
        ]
    ]
];
